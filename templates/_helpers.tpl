{{/*
Expand the name of the chart.
*/}}
{{- define "spring-chart-volume.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "spring-chart-volume.fullname" -}}
{{- if .Values.fullnameOverride }}
{{- .Values.fullnameOverride | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- $name := default .Chart.Name .Values.nameOverride }}
{{- if contains $name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name $name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "spring-chart-volume.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "spring-chart-volume.labels" -}}
helm.sh/chart: {{ include "spring-chart-volume.chart" . }}
{{ include "spring-chart-volume.selectorLabels" . }}
{{- if .Chart.AppVersion }}
app.kubernetes.io/version: {{ .Chart.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "spring-chart-volume.selectorLabels" -}}
app.kubernetes.io/name: {{ include "spring-chart-volume.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}

{{/*
Create the name of the service account to use
*/}}
{{- define "spring-chart-volume.serviceAccountName" -}}
{{- if .Values.serviceAccount.create }}
{{- default (include "spring-chart-volume.fullname" .) .Values.serviceAccount.name }}
{{- else }}
{{- default "default" .Values.serviceAccount.name }}
{{- end }}
{{- end }}



{{/* We will create a pvc template for personal user here */}}
{{- define "mychart.pvc" -}}
{{- if .Values.pvc.enabled }}
apiVersion: v1
kind: PersistentVolumeClaim
metadata: 
    name: {{ .Values.pvc.name }}
spec: 
    accessModes:
{{- toYaml .Values.pvc.accessModes | nindent 4}} 
    storageClassName: {{ .Values.pvc.storageClassName }}       
    resources: 
        requests: 
            storage: {{ .Values.pvc.resources.requests.storage }}

{{- end }}
{{- end }}